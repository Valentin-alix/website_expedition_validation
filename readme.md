Site web avec flask

Comment utiliser l'application web :
1) Obtenir les dépendances : ``pip install -r requirements.txt``
2) Lancer main.py
3) Aller à ``localhost:5000/expedition`` ou ``localhost:5000/validation``

<!>Attention : 
1) L'application marche seulement si l'expéditeur de l'email met une email gmail,
pour mettre une autre addresse email il devra changer le ``app.config['MAIL_SERVER'] = 'smtp.gmail.com'``
2) Vérifier que le compte google de l'expéditeur à "Activer l'accès aux applications moins sécurisées"