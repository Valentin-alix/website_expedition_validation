import smtplib

from flask import Flask, render_template
from flask_mail import Mail, Message

app = Flask(__name__)

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True


@app.route('/validation')
def valide(ortho_name=None):
    return render_template("validation.html", ortho_name=ortho_name)


@app.route('/expedition')
def expédie(ortho_name=None):
    return render_template("expedition.html", ortho_name=ortho_name)


@app.route('/mail_sent/<to>/<from_email>/<password>/<subject>/<ortho_name>/<patients>/<page>')
def send_mail(to, from_email, password, subject, ortho_name, patients, page):
    app.config['MAIL_USERNAME'] = from_email
    app.config['MAIL_PASSWORD'] = password
    mail = Mail(app)
    try:
        patients = list(patients.split(","))
        msg = Message(subject, sender=from_email, recipients=[to])
        msg.html = render_template(page + ".html", ortho_name=ortho_name, patients=patients)
        mail.send(msg)
        return "Envoyé"
    except smtplib.SMTPAuthenticationError or smtplib.SMTPRecipientsRefused:
        return "Erreur d'authentification, vérifier que vous avez bien activé l'accès aux applications moins sécurisées sur le compte google et que vous avez entrez des adresse e-mail valide et le bon mot de passe"


if __name__ == '__main__':
    app.run()
