var liste;
        window.onload = function(){
            liste = document.getElementById("patient_liste");
        }

function add_patient() {
            var newL = document.createElement("li");
            var text = document.createElement("input");
            text.setAttribute('name', 'patient')
            text.type = "text";
            newL.appendChild(text);
            liste.appendChild(newL);
        }

function send_mail() {
    var subject_email = document.getElementById("subject").value;
    var to_email = document.getElementById("to").value;
    var from_email = document.getElementById("from").value;
    var password = document.getElementById("password").value;
    var ortho_name = document.getElementById("ortho_name").value;
    var patients = Array.from(document.getElementsByName("patient"));
    var patients_value = []
    for (patient of patients)
    {
        patients_value.push(patient.value);
    }

    location.href = `/mail_sent/${to_email}/${from_email}/${password}/${subject_email}/${ortho_name}/${patients_value}${window.location.pathname}`
}